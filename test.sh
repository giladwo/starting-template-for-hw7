#!/usr/bin/env bash

cmake ./CMakeLists.txt
make

HW=7

function test_one () {
    if [ -f "tests/hw${HW}q${1}in${2}.txt" ]; then
        echo "=========== TEST $1 $2 ==========="
        diff -y --suppress-common-lines \
            <(cat "tests/hw${HW}q${1}out${2}.txt" | tr -d '\r') \
            <("./hw${HW}q${1}" < "tests/hw${HW}q${1}in${2}.txt" | tr -d '\r') \
            && echo -e "    ✓ PASS"
    else
        echo "skipping missing test $1 $2" >&2
    fi
}

function test_question () {
    tests=$(ls tests/hw${HW}q${1}in*.txt | wc -l)
    for j in $(seq 1 $tests); do
        test_one $1 $j
    done
}

case $# in
    1)
        test_question $1
        ;;
    2)
        test_one $1 $2
        ;;
    *)
        questions=$(ls hw${HW}*.c | wc -l)
        for i in $(seq 1 $questions); do
            test_question $i
        done
        ;;
esac
